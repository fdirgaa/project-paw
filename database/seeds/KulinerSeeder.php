<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class KulinerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Kuliner::class, 5)->create();
    }
}
