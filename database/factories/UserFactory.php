<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

//seeder Kuliner
$factory->define(App\Kuliner::class, function (Faker $faker) {
    return [
        'nama' => $faker->company,
        'kategori' => $faker->randomElement($array = array('makanan', 'minuman')),
        'harga' => $faker->numberBetween($min = 00000000, 99999999),
        'images' => $faker->image(public_path('img'),200,200)
    ];
});