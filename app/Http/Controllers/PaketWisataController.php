<?php

namespace App\Http\Controllers;

use App\PaketWisata;
use Illuminate\Http\Request;

class PaketWisataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paketWisata = PaketWisata::paginate(10);
        $data = ['paket_wisata' => $paketWisata];

        return $data;
    }

    /**
     * tambah the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah(Request $request)
    {
        $paketWisata = new PaketWisata;
        $paketWisata->nama = $request->nama;
        $paketWisata->jml_makan = $request->jml_makan;
        $paketWisata->jml_kamar = $request->jml_kamar;
        $paketWisata->jml_orang = $request->jml_orang;
        $paketWisata->kapasitas_trans = $request->kapasitas_trans;
        $paketWisata->harga = $request->harga;
        $paketWisata->save();

        return $paketWisata;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaketWisata  $paketWisata
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $paketWisata = PaketWisata::find($id);
        $paketWisata->nama = $request->nama;
        $paketWisata->jml_makan = $request->jml_makan;
        $paketWisata->jml_kamar = $request->jml_kamar;
        $paketWisata->jml_orang = $request->jml_orang;
        $paketWisata->kapasitas_trans = $request->kapasitas_trans;
        $paketWisata->harga = $request->harga;
        $paketWisata->save();

        return $paketWisata;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaketWisata  $paketWisata
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $paketWisata = PaketWisata::find(id);
        $paketWisata->delete();

        return $paketWisata();
    }

    /**
     * Detail the specified resource from storage.
     *
     * @param  \App\PaketWisata  $paketWisata
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $paketWisata = PaketWisata::find(id);
        
        return $paketWisata();
    }
}
