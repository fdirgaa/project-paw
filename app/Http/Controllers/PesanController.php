<?php

namespace App\Http\Controllers;

use App\Pesan;
use Illuminate\Http\Request;

class PesanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pesan = Pesan::paginate(10);
        $data = ['pesan' => $pesan];

        return $data;

    }

    /**
     * Tambah the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah(Request $request)
    {
        $pesan = new Pesan;
        $pesan->id_user = $request->id_user;
        $pesan->id_paket = $request->id_paket;
        $pesan->tgl_pesan = $request->tgl_pesan;
        $pesan->tgl_selesai = $request->tgl_selesai;
        $pesan->jml_pesan = $request->jml_pesan;
        $pesan->save();

        return $pesan;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pesan  $pesan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pesan = Pesan::find($id);
        $pesan->id_user = $request->id_user;
        $pesan->id_paket = $request->id_paket;
        $pesan->tgl_pesan = $request->tgl_pesan;
        $pesan->tgl_selesai = $request->tgl_selesai;
        $pesan->jml_pesan = $request->jml_pesan;
        $pesan->save();

        return $pesan;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pesan  $pesan
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $pesan = Pesan::find($id);
        $pesan->delete();

        return $pesan;
    }
}
