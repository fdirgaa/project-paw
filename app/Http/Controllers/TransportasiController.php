<?php

namespace App\Http\Controllers;

use App\Transportasi;
use Illuminate\Http\Request;

class TransportasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transportasi = Transportasi::paginate(10);
        $data = ['transportasi' => $transportasi];

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tembah(Request $request)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);

        $transportasi = new Transportasi;
        $transportasi->nama = $request->nama;
        $transportasi->plat = $request->plat;
        $transportasi->kapasitas = $request->kapasitas;
        $transportasi->harga = $request->harga;
        $transportasi->images = $nama_file;
        $transportasi->save();

        return $transportasi;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transportasi  $transportasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);

        $transportasi = Transportasi::find($id);
        $transportasi->nama = $request->nama;
        $transportasi->plat = $request->plat;
        $transportasi->kapasitas = $request->kapasitas;
        $transportasi->harga = $request->harga;
        $transportasi->images = $nama_file;
        $transportasi->save();

        return $transportasi;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transportasi  $transportasi
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $transportasi = Transportasi::find($id);
        $transportasi->delete();

        return $transportasi;
    }

    /**
     * detail the specified resource from storage.
     *
     * @param  \App\Transportasi  $transportasi
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $transportasi = Transportasi::find($id);
        
        return $transportasi;
    }
}
