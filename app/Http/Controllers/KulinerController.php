<?php

namespace App\Http\Controllers;

use App\Kuliner;
use Illuminate\Http\Request;

class KulinerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kuliner = Kuliner::paginate(10);
        $data = ['kuliner' => $kuliner];

        return $data;
    }

    /**
     * tambah the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah(Request $request)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);

        $kuliner = new Kuliner;
        $kuliner->nama = $request->nama;
        $kuliner->kategori = $request->kategori;
        $kuliner->harga = $request->harga;
        $kuliner->images = $nama_file;
        $kuliner->save();

        return $kuliner;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kuliner  $kuliner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = public_path('img');
        $file->move($tujuan_upload, $nama_file);

        $kuliner = Kuliner::find($id);
        $kuliner->nama = $request->nama;
        $kuliner->kategori = $request->kategori;
        $kuliner->harga = $request->harga;
        $kuliner->images = $nama_file;
        $kuliner->save();

        return $kuliner;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kuliner  $kuliner
     * @return \Illuminate\Http\Response
     */
    public function hapus($id)
    {
        $kuliner = Kuliner::find($id);
        $kuliner = delete();

        return $kuliner;
    }

    /**
     * detail the specified resource from storage.
     *
     * @param  \App\Kuliner  $kuliner
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $kuliner = Kuliner::find($id);

        return $kuliner;
    }
}
