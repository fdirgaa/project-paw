<?php

namespace App\Http\Controllers;

use App\Penginapan;
use Illuminate\Http\Request;

class PenginapanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penginapan = Penginapan::paginate(10);
        $data = ['penginapan' => $penginapan];

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah(Request $request)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);

        $penginapan = new Penginapan;
        $penginapan->nama = $request->nama;
        $penginapan->kategori = $request->kategori;
        $penginapan->nm_fasilitas = $request->nm_fasilitas;
        $penginapan->harga = $request->harga;
        $penginapan->jml_kamar = $request->jml_kamar;
        $penginapan->deskripsi = $request->deskripsi;
        $penginapan->images = $nama_file;
        $penginapan->save();
        return redirect(route('manageroom'))->with('successMsg', 'Data Berhasil di Tambahkan');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Penginapan  $penginapan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'images' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
		$file = $request->file('images');
		$nama_file = time()."_".$file->getClientOriginalName();
		$tujuan_upload = 'img';
        $file->move($tujuan_upload, $nama_file);
        
        $penginapan = Penginapan::find($id);
        $penginapan->nama = $request->nama;
        $penginapan->kategori = $request->kategori;
        $penginapan->nm_fasilitas = $request->nm_fasilitas;
        $penginapan->harga = $request->harga;
        $penginapan->jml_kamar = $request->jml_kamar;
        $penginapan->deskripsi = $request->deskripsi;
        $penginapan->images = $nama_file;        
        $penginapan->save();

        return redirect(route('manageroom'))->with('successMsg', 'Data Berhasil di Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Penginapan  $penginapan
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $penginapan = Penginapan::find($id);
        $penginapan->delete();

        return redirect(route('manageroom'))->with('successMsg', 'Data Berhasil di Delete');
    }

    /**
     * detail the specified resource from storage.
     *
     * @param  \App\Penginapan  $penginapan
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $penginapan = Penginapan::find($id);

        return $penginapan;
    }

    
    public function newroom(){
        $rtype = ['standard', 'premium', 'deluxe', 'junior suite', 'suite', 'presidential'];
        return view('hotelPages/newroom', compact('rtype', 'fasilitas'));
    }
    
    public function editroom($id){
        $rtype = ['standard', 'premium', 'deluxe', 'junior suite', 'suite', 'presidential'];
        $penginapan = Penginapan::find($id);
        return view('hotelPages/updateroom', compact('penginapan','rtype','fasilitas'));
    }
    
    public function manageroom(){
        $penginapan = Penginapan::paginate(10);
        return view('hotelPages/manageroom', compact('penginapan'));
    }
    public function profile(){
        return view('hotelPages/profile');
    }
    public function account(){
        return view('hotelPages/account');
    }
    public function privacy(){
        return view('hotelPages/privacy');
    }
    
}
