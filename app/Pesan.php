<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $table = "pesan";
    public $timestamps = false;

    
    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function paket_wisata()
    {
        return $this->belongsTo('App\PaketWisata');
    }

    public function pembatalan()
    {
        return $this->belongsTo('App\Pembatalan');
    }
    
    
}
