<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaketWisata extends Model
{
    protected $table = "paket_wisata";
    public $timestamps = false;

    public function pesan()
    {
        return $this->hasMany('App\Pesan');
    }
    
}
