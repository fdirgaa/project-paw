<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penginapan extends Model
{
    protected $table = "penginapan";
    public $timestamps = false;
}
