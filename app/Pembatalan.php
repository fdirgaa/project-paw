<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembatalan extends Model
{
    protected $table = "pembatalan";
    public $timestamps = false;

    public function pesan()
    {
        return $this->hasOne('App\Pesan');
    }

    public function user()
    {
        return $this->hasOne('App\User');
    }
}
