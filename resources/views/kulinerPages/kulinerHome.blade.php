@extends('layouts.adminMain')

@section('title', 'Kuliner Page')

@section('sidebar')
@parent

@component('components.sidebar')
@slot('list')
<ul class="components">
    <li>
    <a href="{{ route('kuliner.home') }}" class="{{ (Request::is('kuliner') ? 'active' : '') }}"><i class="fas fa-home ml-3  ml-3"></i><span class="ml-2">Home</span></a>
    </li>
    <li>
        <a href="#submenu1" role="button" aria-controls="submenu1" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-clipboard-list ml-3"></i><span class="ml-2">Menu</span></a>
        <ul class="collapse navbar-nav {{ (Request::is('admin/newroom') ? 'show' : '') }} {{ (Request::is('admin/manageroom') ? 'show' : '') }}" id="submenu1">
            <li class="nav-item">
                <a href="{{ route('newroom') }}"><i class="fas fa-plus-circle ml-3"></i><span class="ml-2">Add New Room</span></a>
            </li>
            <li class="nav-item">
                <a href="{{ route('manageroom') }}"><i class="fas fa-tasks ml-3"></i><span class="ml-2">Manage Room</span></a>
            </li>
        </ul>
    </li>
    <li>
        <a href="{{ route('hotelprofile') }}"><i class="fas fa-user-circle ml-3  ml-3"></i><span class="ml-2">Profile</span></a>
    </li>
    <li>
        <a href="#setting" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-cog ml-3"></i><span class="ml-2">Setting</span></a>
        <ul class="collapse navbar-nav show" id="setting">
            <li class="nav-item">
                <a href="{{ route('hotelaccount') }}"><i class="fas fa-user  ml-3"></i><span class="ml-3">Account</span></a>
            </li>
            <li class="nav-item">
                <a href="{{ route('hotelprivacy') }}"><i class="fas fa-user-shield  ml-3"></i><span class="ml-3">Privacy</span></a>
            </li>
        </ul>
    </li>
</ul>

@endslot
@endcomponent
@endsection

@section('content')
<div class="container">

</div>
@endsection