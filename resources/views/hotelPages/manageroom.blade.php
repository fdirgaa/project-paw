@extends('adminHome')

@section('title', 'Manage Your Room')

@section('content')
@component('components.contentHeader')
@slot('title')
Manage Room
@endslot

@endcomponent
    <div class="container">
        <div class="row justify-content-end my-3">
            <div class="col-lg-2">
            <a href="{{ route('newroom') }}" class="btn btn-success"><i class="fas fa-plus"></i> Add Room</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg">
                @if(session('successMsg'))
            <div class="alert alert-success" role="alert">
                {{ session('successMsg') }}
            </div>
        
            @endif
                <div class="table-responsive table" role="grid">
                    <table class="table">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Room Type</th>
                                <th>Available Room</th>
                                <th>Price</th>
                                <th>Facilities</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="dataKamar">
                            @foreach($penginapan as $kamar)
                            <tr>
                                <td>{{ $kamar->id }}</td>
                                <td>{{ $kamar->kategori }}</td>
                                <td>{{ $kamar->jml_kamar }}</td>
                                <td>{{ $kamar->harga }}</td>
                                <td>{{ $kamar->nm_fasilitas }}</td>
                                <td>
                                <a href="{{ route('editroom', $kamar->id) }}" class="btn btn-success"> <i class="fas fa-edit"></i></a>
                                <a href="{{ route('delete', $kamar->id) }}" class="btn btn-danger"> <i class="fas fa-trash"></i></a>
                                </td>
                            </tr> 
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection