@extends('adminHome')

@section('title', 'Add New Room')


@section('content')

@component('components.contentHeader')
@slot('title')
New Room
@endslot
Add your new rooms here
@endcomponent
<div class="container">
    <div class="row">
        <div class="col-lg">
            @if(session('successMsg'))
            <div class="alert alert-success" role="alert">
                {{ session('successMsg') }}
            </div>
        
            @endif
        <form action="{{ route('penginapan') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" name="nama">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="availableroom">Available Room</label>
                        <input type="text" class="form-control" id="availableroom" name="jml_kamar">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="price24">Price per Night</label>
                        <input type="text" class="form-control" id="price24" name="harga">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="roomtype">Room Type</label>
                        <select class="form-control" id="roomtype" name="kategori">
                            @foreach($rtype as $type)
                            <option value="{{$type}}">{{ ucwords($type) }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-lg">
                        <label for="Facilities">Facilities</label>
                        <input class="form-control" type="text" name="nm_fasilitas">
                    </div>
                </div>
                <div class="form-row">
                    <label for="description">Overview or Description</label>
                    <textarea class="form-control" id="Overvew21" rows="5" name="deskripsi"></textarea><br>
                </div>
                
                        <div class="form-row">
                            <div class="col-lg">
                                <div class="form-group">
                                    <label for="images">Upload Room Image</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="images">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                      </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" name="save">ADD NEW</button>
                        <button type="submit" class="btn btn-primary" name="cancel">CANCEL</button>
            </form>
        </div>

    </div>
</div>
@endsection