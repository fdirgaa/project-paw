@extends('layouts.app')

@section('title', 'Daftar Akun Baru')

@section('content')
<div class="container">
    <div class="row justify-content-center text-center">
        <div class="col-lg-8">
            <form action="{{ route('register') }}" method="post">
            <div class="card bg-dark text-white">
                {{-- <img class="text-center card-img-top img-fluid w-25" src="{{ asset('img/blue_plane_icon.png') }}" alt="Logo"> --}}
                <div class="card-body">
                    <h4 class="card-title">Daftarkan Akun Anda!</h4>
                    <p class="card-text">Gratis</p>
                        @csrf
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Account Type</label>

                            <div class="col-md-6">
                                <select name="tipeAkun" id="tipeAkun" class="form-control">
                                    <option value="0">User</option>
                                    <option value="2">Tenant</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row text-center mb-0">
                            <div class="col-lg ">
                                <button type="submit" class="btn btn-primary align-self-center">
                                    {{ __('Register') }}
                                </button><br>
                            <a href="{{ route('login') }}" class="card-link">Sudah Punya Akun?</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        </div>
</div>
</div>
@endsection