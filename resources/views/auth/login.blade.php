@extends('layouts.app')

@section('title', 'Login')

@section('content')
<div class="container text-white">
    <div class="row justify-content-center my-5">
        <div class="col-4 align-self-center align-middle">
            <div class="box p-2 rounded-15">
                <div class="row my-2">
                    <img src="{{ asset('img/logo_putih.png') }}" alt="Logo ITril" class="img img-fluid w-50 mx-auto">
                </div>
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="row justify-content-center">
                        <div class="col-11">
                            <div class="form-group">
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Username/Email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group text-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-success">Sign In</button>
                        </div>
                    </div>
                    <div class="row text-center">
                        <div class="col">
                            @if (Route::has('password.request'))
                                <a class="text text-success" href="{{ route('password.request') }}">
                                    Lupa Kata Sandi?
                                </a>
                                @endif

                            <p >Tidak Punya Akun? <a href="{{ route('register') }}" class="text-success">Daftar</a></p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection