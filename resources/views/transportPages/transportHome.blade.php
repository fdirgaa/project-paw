@extends('layouts.adminMain')

@section('title', 'Transport Page')

@section('sidebar')
@parent

@component('components.sidebar')
@slot('list')
<ul class="navbar-nav">
    <li>
    <a href="{{ route('transport.home') }}" class="{{ (Request::is('transport') ? 'active' : '') }}"><i class="fas fa-home ml-3  ml-3"></i><span class="ml-2">Home</span></a>
    </li>
    <li>
        <a href="{{ route('hotelprofile') }}" class="{{ (Request::is('transport/profile') ? 'active' : '') }}"><i class="fas fa-cog ml-3  ml-3"></i><span class="ml-2">Profile</span></a>
    </li>
    <li>
        <a href="#setting" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-circle ml-3"></i><span class="ml-2">Setting</span></a>
        <ul class="collapse navbar-nav {{ (Request::is('transport/account') ? 'show' : '') }} {{ (Request::is('transport/privacy') ? 'show' : '') }}" id="setting">
            <li class="nav-item">
                <a href="{{ route('hotelaccount') }}" class="{{ (Request::is('transport/account') ? 'active' : '') }}"><i class="fas fa-user  ml-3"></i><span class="ml-3">Account</span></a>
            </li>
            <li class="nav-item">
                <a href="{{ route('hotelprivacy') }}" class="{{ (Request::is('transport/privacy') ? 'active' : '') }}"><i class="fas fa-user-shield  ml-3"></i><span class="ml-3">Privacy</span></a>
            </li>
        </ul>
    </li>
</ul>

@endslot
@endcomponent
@endsection

@section('content')
<div class="container">

</div>
@endsection