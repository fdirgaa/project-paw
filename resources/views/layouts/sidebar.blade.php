<!-- Start Sidebar  -->
<nav id="sidebar" class="d-md-block">
            <div class="sidebar-header">
                <h3>ITril</h3>
            </div>

            @yield('list')

            <ul class="list-unstyled CTAs">
                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                            <i class="fas fa-sign-out-alt" aria-hidden="true"></i><span class="ml-3">Logout</span></i></a>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </ul>
        </nav>
        <!-- End Sidebar  -->