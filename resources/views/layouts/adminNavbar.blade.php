<nav class="navbar navbar-expand-lg navbar-light bg-white" id="navigation">
        <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-info d-sm-block d-lg-none">
                <i class="fas fa-align-left"></i>
                <span>Toggle Sidebar</span>
            </button>
            <a href="#" class="navbar-brand ml-2">
                <img src="{{ asset('img/blue_plane_icon.png') }}" alt="logo" width="40" class="d-inline-block align-top"><span class="ml-2">ITril</span>
            </a>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-justify"></i>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#"><i class="fas fa-search"></i> <span class="ml-2 d-inline-block d-lg-none"> Search</span></a>
                    </li>
                    <li class="nav-item ml-3">
                        <a class="nav-link" href="#"><i class="fas fa-bell"></i><span class="ml-2 d-inline-block d-lg-none"> Notification</span></a>
                    </li>

                  <li class="nav-item dropdown ml-3">
                    <a class="nav-link dropdown-toggle" id="navbarProfileMenuLink" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i><span class="ml-2 d-inline-block d-lg-none">Profile</span></a>
                        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarProfileMenuLink">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt" aria-hidden="true"></i><span class="ml-3">Logout</span></i></a>
                        <a class="dropdown-item d-none" href="#">Another action</a>
                        </div>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </li>
                    <li class="nav-item ml-3 mr-5">
                        <a class="nav-link" href="#"><i class="fas fa-cog"></i><span class="ml-2 d-inline-block d-lg-none">  Setting</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>