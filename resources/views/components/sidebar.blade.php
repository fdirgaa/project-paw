<!-- Start Sidebar  -->
<nav id="sidebar" class="d-md-block">
    <div class="row">
        <div class="col sidebar-header h-25">
            <img  src="#" alt="Photo Profile" class="img-fluid rounded-circle" >
        </div>
    </div>

    {{$list}}

    <ul class="navbar-nav">
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt ml-3" aria-hidden="true"></i><span class="ml-2">Logout</span></i></a>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </ul>
</nav>
<!-- End Sidebar  -->