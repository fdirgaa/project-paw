<div class="jumbotron jumbotron-fluid no-gutters p-2" >
    <div class="container no-gutters">
        <h1 class="display-4">{{ $title }}</h1>
        <p class="lead">{{$slot}}</p>
    </div>
</div>