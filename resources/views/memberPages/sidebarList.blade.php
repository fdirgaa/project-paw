<ul class="list-unstyled CTAs">
      <li>
        <a href="#pageProfile" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-user-circle"></i><span class="ml-2">Profile</span></a>
        <ul class="collapse list-unstyled" id="pageProfile">
            <li>
                <a href="#"><i class="fas fa-user"></i><span class="ml-2">Account</span></a>
            </li>
            <li>
                <a href="#"><i class="fas fa-user-shield"></i><span class="ml-2">Privacy</span></a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#"><i class="fas fa-cog"></i><span class="ml-2">Setting</span></a>
    </li>
</ul>