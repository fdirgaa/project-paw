$(document).ready(function() {
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });

    $('#sidebarCollapse').on('click', function() {
        $('#sidebar, #content').toggleClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});


// Add Facilities
var x = 0;
var fasName = Array();
var fasCat = Array();

function addFas() {
    fasName[x] = document.getElementById("fasName").value;
    fasCat[x] = document.getElementById("fasCat").value;
    x++;
    document.getElementById("fasName").value = "";
    var tableBody = "";
    for (var i = 0; i < fasName.length; i++) {
        tableBody += "<tr><td>" + fasName[i] + "</td><td>" + fasCat[i] + "</td></tr>";
    }
    document.getElementById('fasData').innerHTML = tableBody;
    fName = fasName.join();
    fCat = fasCat.join();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        url: 'http://127.0.0.1:8000/api/penginapan',
        data: { fName: 'nm_fasilitas', fCat: 'kt_fasilitas' },
        type: 'POST',
        dataType: 'json'
    });
}

function clearFas() {
    document.getElementById("fasName").value = "";
}
// End Add Facilities

function tampil() {
    var dat = document.getElementById('data');
    dat.innerHTML = "<p>Nama Fasilitas : " + fName + " dan Kategori Fasilitas : " + fCat;
}
