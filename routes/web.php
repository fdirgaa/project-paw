<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('wlcm');

// Auth Controller
Auth::routes();

// Home Controller
Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');
Route::get('kuliner', 'HomeController@kuliner')->name('kuliner.home')->middleware('is_admin');
Route::get('transport', 'HomeController@transport')->name('transport.home')->middleware('is_admin');


Route::get('admin/newroom', 'PenginapanController@newroom')->name('newroom')->middleware('is_admin');
Route::get('admin/manageroom', 'PenginapanController@manageroom')->name('manageroom')->middleware('is_admin');
Route::post('admin/penginapan', 'PenginapanController@tambah')->name('penginapan')->middleware('is_admin');
Route::get('admin/editroom/{id}', 'PenginapanController@editroom')->name('editroom')->middleware('is_admin');
Route::get('admin/delete/{id}', 'PenginapanController@delete')->name('delete')->middleware('is_admin');
Route::post('admin/penginapan/{id}', 'PenginapanController@update')->name('updatekamar')->middleware('is_admin');
Route::get('admin/profile', 'PenginapanController@profile')->name('hotelprofile')->middleware('is_admin');
Route::get('admin/account', 'PenginapanController@account')->name('hotelaccount')->middleware('is_admin');
Route::get('admin/privacy', 'PenginapanController@privacy')->name('hotelprivacy')->middleware('is_admin');







