<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// RestApi Kuliner
Route::get('kuliner', 'KulinerController@index');
Route::post('kuliner', 'KulinerController@tambah');
Route::get('/kuliner/{id}', 'KulinerController@detail');
Route::put('/kuliner/{id}', 'KulinerController@update');
Route::delete('/kuliner/{id}', 'KulinerController@hapus');

// RestApi Transportasi
Route::get('transportasi', 'TransportasiController@index');
Route::post('transportasi', 'TransportasiController@tambah');
Route::get('/transportasi/{id}', 'TransportasiController@detail');
Route::put('/transportasi/{id}', 'TransportasiController@update');
Route::delete('/transportasi/{id}', 'TransportasiController@hapus');

// RestApi Penginapan
Route::get('penginapan', 'PenginapanController@index');
Route::post('penginapan', 'PenginapanController@tambah');
Route::get('/penginapan/{id}', 'PenginapanController@detail');
Route::post('/penginapan/{id}', 'PenginapanController@update');
Route::delete('/penginapan/{id}', 'PenginapanController@hapus');

// RestApi Paket Wisata
Route::get('paketWisata', 'PaketWisataController@index');
Route::post('paketWisata', 'PaketWisataController@tambah');
Route::get('/paketWisata/{id}', 'PaketWisataController@detail');
Route::put('/paketWisata/{id}', 'PaketWisataController@update');
Route::delete('/paketWisata/{id}', 'PaketWisataController@hapus');

// RestApi Pesan
Route::get('pesan', 'PesanController@index');
Route::post('pesan', 'PesanController@tambah');
// Route::get('/pesan/{id}', 'PenginapanController@detail');
Route::put('/pesan/{id}', 'PesanController@update');
Route::delete('/pesan/{id}', 'PesanController@hapus');